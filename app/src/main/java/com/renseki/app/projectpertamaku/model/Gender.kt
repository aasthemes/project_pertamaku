package com.renseki.app.projectpertamaku.model

enum class Gender {
    MALE, FEMALE
}